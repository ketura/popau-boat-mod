package com.ketura.popau;

import net.minecraft.block.Block;
import net.minecraft.block.Block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class GenericBlock extends Block
{
  public GenericBlock (Material material)
  {
    super(material);
  }
  
  public GenericBlock(Material mat, float hardness, SoundType sound, String name, CreativeTabs tab)
  {
    super(mat);
    this.setHardness(hardness);
    this.setStepSound(sound);
    this.setBlockName(name);
    this.setCreativeTab(tab);
  }

}

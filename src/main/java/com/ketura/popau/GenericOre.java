package com.ketura.popau;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.Block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class GenericOre extends Block
{
  public GenericOre (Material material)
  {
    super(material);
    this.setHardness(4.0F);
    this.setStepSound(Block.soundTypePiston);
    this.setBlockName("genericOre");
    this.setCreativeTab(CreativeTabs.tabBlock);
    this.setHarvestLevel("pickaxe", 3);
  }
  
  @Override
  public Item getItemDropped(int metadata, Random random, int fortune) {
      return Popau.genericIngot;
  }
  
  @Override
  public int quantityDropped(Random rand)
  {
    return 25;
  }
  
  /*
  //If the block's drop is an item.
  @Override
  public Item getItemDropped(int metadata, Random random, int fortune) {
      return Generic.genericIngot;
  }
  
  //If the block's drop is a block.
  @Override
  public Item getItemDropped(int metadata, Random random, int fortune) {
      return Item.getItemFromBlock(Generic.genericBlock);
  }
  
  //If the block's drop is a vanilla item.
  @Override
  public Item getItemDropped(int metadata, Random random, int fortune) {
      return Item.getItemById(Id);
  }
  
  //If the block's drop is a vanilla block.
  @Override
  public Item getItemDropped(int metadata, Random random, int fortune) {
      return Item.getItemFromBlock(Block.getBlockById(id));
  }
  */
}

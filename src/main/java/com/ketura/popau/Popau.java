package com.ketura.popau;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.block.material.Material;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler; // used in 1.6.2
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = Popau.MODID, name="Popau Boat Mod", version = Popau.VERSION)

public class Popau 
{
  
  public static final String MODID = "popau";
  public static final String VERSION = "1.7.10-0.1.1.1"; //@MAJOR@.@MINOR@.@REV@.@BUILD@
  
  public static Item genericItem;
  public static Item genericIngot;
  
  public static Block genericDirt;
  public static final Block genericOre = new GenericOre(Material.rock);;
  
  
  
  @Instance(value=MODID)
  public static Popau instance;
  
  @SidedProxy(clientSide="com.ketura.popau.client.ClientProxy", serverSide="com.ketura.popau.CommonProxy")
  public static CommonProxy proxy;
  
  @EventHandler
  public void preInit(FMLPreInitializationEvent event)
  {
    genericItem = new GenericItem();
    genericIngot = new GenericItem(16, CreativeTabs.tabMisc, 1, "genericIngot");
    genericDirt = new GenericBlock(Material.ground, 0.5F, Block.soundTypeGravel, "genericDirt", CreativeTabs.tabBlock);
    GameRegistry.registerItem(genericItem, "genericItem");
    GameRegistry.registerItem(genericIngot, "genericIngot");
    GameRegistry.registerBlock(genericDirt, "genericDirt");

    GameRegistry.registerBlock(genericOre, "genericOre");
    
    genericDirt.setHarvestLevel("shovel", 0);
    
  }
  
  @EventHandler
  public void load(FMLInitializationEvent event)
  {
    proxy.registerRenderers();
  }
  
  @EventHandler
  public void postInit(FMLPostInitializationEvent event)
  {
    //stub
  }
  
  

}

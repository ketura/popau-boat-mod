package com.ketura.popau;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class GenericItem extends Item
{
  public GenericItem()
  {
    setMaxStackSize(64);
    setCreativeTab(CreativeTabs.tabMisc);
    setUnlocalizedName("genericItem");
  }
  
  public GenericItem(int maxStackSize, CreativeTabs tab, int texture, String name)
  {
    setMaxStackSize(maxStackSize);
    setCreativeTab(tab);
    setUnlocalizedName(name);
  }
}
